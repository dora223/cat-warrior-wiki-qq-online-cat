银河星带
欢迎使用猫武士维基QQ在线，请您发送 搜索 武士名（部分猫以最终名为准） 语录 武士名（特殊同上）
如果暂未找到您所查询的，可能是我们正在补充，欢迎您提供链接和图片，让我们提前上传。
如果机器人不回复可稍等，或发送 银河星带 确认是否在线，如果在下时间掉线，请@银杏掌。
机器人开放时间（不完全准确）
星期一-星期五 19:00--22:00
周六周日或全天或半天开放。
每一颗星星都是武士们的祖先--猫语录


雷族的大门永远向你敞开-灰条
没有什么能比当巫医为族群服务更让我快乐了-叶池

猫武士维基
猫武士维基是面向中国大陆基于灰机wiki的电子百科。网站地址（机器人基本来源）http://warriors.huijiwiki.com/wiki/

测试
±img=https://a.download.yunzhongzhuan.com/download/171990/sZZfFs4I/Stitch_20230219_182228.png

搜索 灰条
由于一些不可抗力因素，机器人只截止到生平，请自行前往猫武士维基百科查询
±img=https://i.328888.xyz/2023/02/19/XgzjJ.jpeg

语录 灰条
查询当前语录较多，请选择。（）不用打
语录 灰条1（呼唤野性）         
    语录 灰条2（寒冰烈火）       
  语录 灰条3（寒冰烈火和火心）     
  语录 灰条4（风起云涌）    
  语录 灰条5（河族阴云）        
  语录 灰条6（日落和平）  
语录 灰条7（灰条的誓言）  
    语录 灰条8（灰条的誓言）  
x6限制剩余条目需再次发送 语录 灰条目录2

语录 灰条目录2
语录 灰条9（暗夜密语/星夜私语）
语录 灰条10（黑莓星的风暴）
语录 灰条11（暗夜幻象）
语录 灰条12（迷雾明光）
语录 灰条13（灰条的誓言）
语录 灰条14（灰条的誓言）
语录 灰条15（灰条的誓言）
语录 灰条16（灰条的誓言）
语录 灰条17（灰条的誓言）

语录 灰条1
這世上再也沒有比當寵物貓更悲慘的事情了，他們只是兩腳獸的玩具而已！專吃一些不像食物的食物，只能在沙盆裡拉屎。除非兩腳獸答應他們，否則他們不敢把鼻子探出室外，真是一點生命力也沒有！而這外頭可自由多了，完全不受拘束，愛去哪兒就去哪兒。你要是沒嚐過現宰的老鼠，這輩子就等於白活了。
—— 灰条對拉斯特，在他們第一次見面時，《荒野新生》，第30頁

语录 灰条2
灰紋
多謝！
銀流
你這個白痴！你到我們的地盤來幹什麼？
灰紋
淹死啊？
銀流
你不能在你們的地盤裡淹死嗎？
灰紋
啊！那裡有誰能救我呢？
—— 銀流從水中救出灰紋後，《烈火寒冰》，第121-122頁

语录 灰条3
這是你自己必須面對的事，我沒資格說什麼。灰紋，不管你決定怎麼做，我永遠都是你的朋友。
—— 火心對灰紋，《烈火寒冰》，第283頁

语录 灰条4
火心
我很想念你，你為什麼不回來？
灰紋
我不能丟下我的孩子。（…）噢，他們在育兒室受到很好的照顧。他們待在河族會很安全又幸福。不過我不認為我狠得下心離開他們。他們會使我想起銀流的點點滴滴。
火心
你那麼懷念她？
灰紋
我深愛她。
—— 火心勸灰紋回歸雷族，《風暴將臨》，第155

语录 灰条5
±img=https://s1.ax1x.com/2023/02/21/pSjMWSx.png

语录 灰条6
你必須做你認為最好的事，但我知道一件事：對灰紋來說最重要的，就是你的友誼和這個族。就連他還在河族的時候，他都渴望著回家。他會想看到雷族強盛壯大，即使這表示接受他無法回來的事實。
—— 暴毛告訴火星灰紋內心的想法，《日落和平》，第224頁

语录 灰条7
±img=https://s1.ax1x.com/2023/02/21/pSjMh6K.png

语录 灰条8
±img=https://pic.imgdb.cn/item/63f4bda6f144a010079e5499.png

语录 灰条9
±img=https://pic.imgdb.cn/item/63f4bda7f144a010079e56e8.png

语录 灰条10
±img=https://pic.imgdb.cn/item/63f4be52f144a010079f6015.png

语录 灰条11
±img=https://s1.ax1x.com/2023/02/21/pSjMOpt.png

语录 灰条12
我已經度過了有意義的一生，擁有過親密的摯友和忠誠的族伴。我愛過、被愛過，還有了令我自豪的孩子們。沒有什麼比這些更令一名戰士滿足了。我很感激能擁有這最後一次保衛部族的機會。暫譯
—— 灰紋的遺言，《迷霧之光》英文版， 第307頁

语录 灰条13
守則教導我們，打仗是為了展示力量和保衛家園，而不是彼此相互殘殺。暫譯
—— 灰紋對戰士守則的看法，《灰紋的誓言》英文版，第125頁

语录 灰条14
暴毛
（羽尾）是那麼的陽光熱情、坦蕩寬容。暫譯
灰紋
是啊，她總能讓我想起銀流，你們那從未謀面的母親，也是我第一隻愛上的貓。後來我又有了一位伴侶，她叫蜜妮，但她現也已經病逝。暫譯
暴毛
對不起。我不敢想像失去溪兒會是什麼樣，更別說失去兩位伴侶了。暫譯
—— 灰紋和暴毛，《灰紋的誓言》英文版，第121頁

语录 灰条15
守則教導我們，打仗是為了展示力量和保衛家園，而不是彼此相互殘殺。暫譯
—— 灰紋對戰士守則的看法，《灰紋的誓言》英文版，第125頁

语录 灰条16
±img=https://s1.ax1x.com/2023/02/21/pSjQp7Q.png

语录 灰条17
如果我現在回去，我就又得被長老的身份所困，他想。而那可能就是我的全部了。但在我踏上回雷族的路之前，我必須弄明白我應當做些什麼。暫譯
—— 灰紋的想法，在離開部落繼續旅程之前，《灰紋的誓言》英文版，第129頁

语录 夜星
https://pic.imgdb.cn/item/642051d6a682492fcc204618.jpg

语录 火星
本次采取2023.2.25，可能已经有新的内容。
±img=https://pic.imgdb.cn/item/63fa00c1f144a01007810c69.jpg

语录 火爪
±img=https://pic.imgdb.cn/item/63fa00c1f144a01007810c69.jpg

语录 高星
当前采集日期为2023.2.25，可能部分内容失效。
±img=https://pic.imgdb.cn/item/63fa00c2f144a01007810e53.jpg

语录 沙风
（图片更新为2.25）
±img=https://pic.imgdb.cn/item/63fa00c1f144a01007810cb0.jpg

语录 炭毛
±img=https://pic.imgdb.cn/item/63fa00c2f144a01007810fb9.jpg

搜索 火星
由于一些不可抗力因素，目前机器人只支持生平后续版本开放，完整内容至https://warriors.huijiwiki.com/wiki/火星
±img=https://i.328888.xyz/2023/02/19/XgJfz.jpeg

搜索 火爪
由于一些不可抗力因素，目前机器人只支持生平后续版本开放，完整内容至https://warriors.huijiwiki.com/wiki/火星
±img=https://i.328888.xyz/2023/02/19/XgJfz.jpeg

搜索 云尾
因为技术原因，当前版本只支持生平，后续将陆续开放，可到猫武士维基百科查询完整内容https://warriors.huijiwiki.com/wiki/云尾
±img=https://pic.imgdb.cn/item/63f23d6ef144a0100752ba71.jpg

搜索 斯玛
因为技术原因，当前版本只支持生平，后续将陆续开放，可到猫武士维基百科查询完整内容https://warriors.huijiwiki.com/wiki/斯玛
±img=https://pic.imgdb.cn/item/63f23d6ef144a0100752b9d2.jpg

搜索 乌爪
因为技术原因，当前版本只支持生平，后续将陆续开放，可到猫武士维基百科查询完整内容https://warriors.huijiwiki.com/wiki/乌爪
±img=https://pic.imgdb.cn/item/63f23d6ef144a0100752b965.jpg

搜索 斑叶
因为技术原因，当前版本只支持生平，后续将陆续开放，可到猫武士维基百科查询完整内容https://warriors.huijiwiki.com/wiki/斑叶
±img=https://pic.imgdb.cn/item/63f2428ff144a010075bf90f.jpg

搜索 蓝星
因为技术原因，当前版本只支持生平，后续将陆续开放，可到猫武士维基百科查询完整内容https://warriors.huijiwiki.com/wiki/蓝星
±img=https://pic.imgdb.cn/item/63f24292f144a010075bfcf7.jpg

搜索 银溪
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容 http://warriors.huijiwiki.com/wiki/银溪
±img=https://s1.ax1x.com/2023/02/20/pSXVQeI.jpg

搜索 虎星
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容
±img=https://s1.ax1x.com/2023/02/20/pSXZwge.jpg

搜索 虎掌
因为技术原因，当前版本只支持生平，后续将陆续开放，可到猫武士维基百科查询完整内容
±img=https://s1.ax1x.com/2023/02/20/pSXZwge.jpg

搜索 狮心
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容
http://warriors.huijiwiki.com/wiki/狮心
±img=https://s1.ax1x.com/2023/02/20/pSXe3RS.jpg

搜索 金花
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/金花
±img=https://s1.ax1x.com/2023/02/20/pSXehi6.jpg

搜索 白风
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/白风
±img=https://s1.ax1x.com/2023/02/20/pSXeLdI.jpg

搜索 黑条
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/黑条
±img=https://s1.ax1x.com/2023/02/20/pSXmThV.jpg

搜索 奔风
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/奔风
±img=https://s1.ax1x.com/2023/02/20/pSXnStx.jpg

搜索 柳带
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/柳带
±img=https://s1.ax1x.com/2023/02/20/pSXnmNt.jpg

搜索 鼠毛
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容
http://warriors.huijiwiki.com/wiki/鼠毛
±img=https://s1.ax1x.com/2023/02/20/pSXnfgO.jpg

搜索 尘毛
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/尘毛
±img=https://pic.imgdb.cn/item/63f6091cf144a010077210d5.jpg

搜索 霜毛
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容
http://warriors.huijiwiki.com/wiki/霜毛
±img=https://pic.imgdb.cn/item/63f6091bf144a01007720fe3.jpg

搜索 纹脸
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容
http://warriors.huijiwiki.com/wiki/纹脸
±img=https://pic.imgdb.cn/item/63f6091bf144a01007720f48.jpg

搜索 斑尾
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/斑尾
±img=https://pic.imgdb.cn/item/63f6091bf144a01007720f26.jpg

搜索 一只眼
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/一只眼
±img=https://pic.imgdb.cn/item/63f8aa3cf144a0100786a7c4.png

搜索 半尾
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/半尾
±img=https://pic.imgdb.cn/item/63f8aa3ef144a0100786a9e2.png

搜索 小耳
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容
http://warriors.huijiwiki.com/wiki/小耳
±img=https://pic.imgdb.cn/item/63f8aa3df144a0100786a8c2.png

搜索 团毛
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/团毛
±img=https://pic.imgdb.cn/item/63f8aa3cf144a0100786a7ef.png

搜索 沙风
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/沙风
±img=https://pic.imgdb.cn/item/63f8aa3ef144a0100786aa75.png

搜索 断星
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/断星
±img=https://pic.imgdb.cn/item/64104fdeebf10e5d53381fb2.jpg

搜索 灰毛
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/灰毛
±img=https://pic.imgdb.cn/item/642051d4a682492fcc2042da.jpg

搜索 亮花
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/亮花
±img=https://pic.imgdb.cn/item/642051d4a682492fcc20443a.jpg

搜索 曙云
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/曙云
±img=https://pic.imgdb.cn/item/642051d6a682492fcc204564.jpg

搜索 夜爪
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/夜星
±img=https://pic.imgdb.cn/item/642051d7a682492fcc2047cb.jpg

搜索 夜皮
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/夜皮 夜皮语录请输入 语录夜星
±img=https://pic.imgdb.cn/item/642051d7a682492fcc2047cb.jpg

搜索 夜星
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/夜星 语录部分请输入 语录 夜星
±img=https://pic.imgdb.cn/item/642051d7a682492fcc2047cb.jpg

搜索 爪脸
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/爪脸
±img=https://pic.imgdb.cn/item/642051e6a682492fcc206236.jpg

搜索 石头
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/石头
±img=https://pic.imgdb.cn/item/642051e8a682492fcc20647d.png

搜索 短尾
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/短尾
±img=https://pic.imgdb.cn/item/642051eaa682492fcc2067af.jpg

搜索 奔鼻
±img=https://pic.imgdb.cn/item/642051eba682492fcc206914.jpg

搜索 黑星
因为技术原因，当前版本只支持部分内容，后续将陆续开放，可到猫武士维基百科查询完整内容http://warriors.huijiwiki.com/wiki/黑星
±img=https://pic.imgdb.cn/item/642051eea682492fcc206e62.jpg


你是机器人吗
是，完全是机器人。仅调用猫武士维基百科内容，目的为更加方便查询，机器人没有任何情感，不是ai。如果猫武士灰机wiki错了机器人也会错的。

怎么办
遵从你的内心

欢迎
愿星族照亮你前进的道路，星族的大门永远向你敞开。

加油
愿星光为你指引方向

死
一起会好起来的（抱抱(づ′▽`)づ）
±img=https://s1.ax1x.com/2023/02/20/pSOgDeg.png

星族在哪
±img=https://s1.ax1x.com/2023/02/20/pSOgbf1.png

呼唤野性英国图书 封面
±img=https://huiji-public.huijistatic.com/warriors/uploads/a/aa/PB_1_cover.jpg

愿星族指引你前行的道路
遵从你内心的选择，星族的大门永远向你敞开。

刀
一切会好起来的。
±img=https://s1.ax1x.com/2023/02/20/pSOgDeg.png